# 0x42 

## Installation

1. Downloading dependencies from poetry

   ```
   poetry install
   ```

2. Setup config
    - Make a new file `config.yml`
    - Fill `config.yml` which the snippet provided below
    
    ```yaml
    BOT:
      token: "Bot token here"
      prefix: "Prefix"
      colour: # Colour to use for the bot. ex: 0xf09892
      owner_ids:
        - 123
        - 456
        # etc

    DB:
      name: "database name" 
      user: "database user"
      password: "database password"
    ```
3. Running the bot

   ```
   poetry run loader
   ```
   Aftwards bot should spit out a bunch of information meaning its on

## Development
_For developers_
If you plan on contributing please open an issue beforehand
for discussion or otherwise contact me via discord.`0x41#0042`
Make sure to follow the `style-guide` when writing code

## Contributors

- [an-dyy](https://github.com/an-dyy) - creator and maintainer

