import asyncio
import contextlib

import src


async def main() -> None:
    await src.Bot().start()


def loader() -> None:
    with contextlib.suppress(KeyboardInterrupt):
        asyncio.run(main())
