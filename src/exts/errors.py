from __future__ import annotations

import typing as t
import traceback
import difflib

import discord
from discord.ext import commands

from ..core import Context

if t.TYPE_CHECKING:
    from ..core import Bot


class Errors(commands.Cog):
    IGNORED_ERRORS: t.ClassVar[t.Tuple[t.Type[commands.CommandError], ...]] = (
        commands.NotOwner,
    )

    """
    A cog used to handle errors
    """

    def __init__(self, bot: Bot, command_attrs={"hidden": True}):
        self.bot = bot
        self.original_handler = bot.on_command_error
        bot.on_command_error = self.on_command_error  # type: ignore

    async def on_command_error(
        self, ctx: Context, error: commands.CommandError
    ) -> t.Optional[discord.Message]:
        """
        A method used to handle any errors raised

        Args:
            ctx (Context): Context corresponding to the error's invocation
            error (commands.CommandError): The error rai
        """
        if not isinstance(error, Errors.IGNORED_ERRORS):
            if isinstance(error, commands.CommandNotFound):
                if close_matches := difflib.get_close_matches(
                    ctx.message.content,
                    [
                        command.name
                        for command in self.bot.commands
                        if not command.hidden
                    ],
                    cutoff=0.5,
                ):
                    formatted = "\n".join(close_matches)
                    return await ctx.reply(
                        embed=discord.Embed(
                            title=f"Command {ctx.message.content} not found",
                            description=f"Did you mean:\n `{formatted}`",
                            colour=discord.Colour.red(),
                        )
                    )

                return await ctx.reply(
                    embed=discord.Embed(
                        title=f"Command {ctx.message.content} not found",
                        colour=discord.Colour.red(),
                    )
                )

        trace = traceback.format_exception(type(error), error, error.__traceback__)
        self.bot.logger.error(
            f"{ctx.command.qualified_name if ctx.command else ctx.message.content} -> {''.join(trace)}"
        )

    def cog_unload(self) -> None:
        self.bot.on_command_error = self.original_handler


def setup(bot: Bot) -> None:
    return bot.add_cog(Errors(bot))
