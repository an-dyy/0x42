from __future__ import annotations

import typing as t

import datetime

import discord
from discord.ext import commands, tasks


from ..utils import TimeConverter, hierarchy_check, HyperView
from ..core import Context

if t.TYPE_CHECKING:
    from ..core import Bot


class Mod(commands.Cog):
    def __init__(self, bot: Bot):
        self.bot = bot
        self.check_mutes.start()

    async def cog_check(self, ctx: Context) -> bool:
        return await commands.guild_only().predicate(ctx)

    async def get_mute_role(self, guild_id: int) -> t.Optional[discord.Role]:
        mute_role_id = await self.bot.database.fetchval(
            "SELECT mute_role FROM guilds WHERE id = $1", guild_id
        )

        if guild := self.bot.get_guild(guild_id):
            if mute_role := discord.utils.get(guild.roles, id=mute_role_id):
                return mute_role
            return None
        else:
            return None

    @tasks.loop(seconds=10)
    async def check_mutes(self) -> None:
        await self.bot.wait_until_ready()

        for muted_user, data in self.bot.muted_users.copy().items():
            muted_at, mute_seconds, guild_id = data
            guild = self.bot.get_guild(guild_id)
            member = guild.get_member(muted_user)  # type: ignore

            if datetime.datetime.utcnow() >= (
                muted_at + datetime.timedelta(seconds=mute_seconds)
            ):
                self.bot.dispatch("member_unmute", member)

    @commands.Cog.listener()
    async def on_member_unmute(self, member: discord.Member) -> None:
        await self.bot.database.execute("DELETE FROM mutes WHERE target_id = $1", member.id)  # type: ignore
        if mute_role := await self.get_mute_role(member.guild.id):
            await member.remove_roles(mute_role)  # type: ignore

        self.bot.muted_users.pop(member.id)
        self.bot.logger.info(f"User unmuted: {member.id}")

    @commands.Cog.listener()
    async def on_member_join(self, member: discord.Member) -> None:
        if member.id in self.bot.muted_users:
            if mute_role := await self.get_mute_role(member.guild.id):
                await member.add_roles(mute_role)  # type: ignore

    @commands.command()
    @commands.has_permissions(manage_roles=True, manage_messages=True)
    @hierarchy_check
    async def mute(
        self, ctx: Context, target: discord.Member, duration: TimeConverter
    ) -> t.Optional[discord.Message]:
        if await self.bot.database.fetchrow(
            "SELECT target_id FROM mutes WHERE target_id = $1", target.id
        ):
            return await ctx.reply("Cannot mute an already muted member")

        if mute_role := await self.get_mute_role(ctx.guild.id):  # type: ignore
            await target.add_roles(mute_role)  # type: ignore
            await self.bot.database.execute(
                "INSERT INTO mutes (target_id, guild_id, muted_at, mute_seconds) VALUES ($1, $2, $3, $4)",
                target.id,
                target.guild.id,
                datetime.datetime.utcnow(),
                duration.raw,  # type: ignore
            )
            await ctx.reply(f"Muted {target} for {duration.readable}")  # type: ignore
            self.bot.muted_users[target.id] = (datetime.datetime.utcnow(), duration.raw, ctx.guild.id)  # type: ignore
        else:
            await ctx.reply(
                f"No mute role was set for this guild. You can set a mute role via `{ctx.prefix}set mute <id>`"
            )

    @commands.command()
    @commands.has_permissions(manage_roles=True, manage_messages=True)
    async def unmute(
        self, ctx: Context, target: discord.Member
    ) -> t.Optional[discord.Message]:
        if await self.bot.database.fetchrow(
            "SELECT target_id FROM mutes WHERE target_id = $1", target.id
        ):
            self.bot.dispatch("member_unmute", target)
            return await ctx.send(f"{target} has been unmuted")

    @commands.command()
    @commands.has_permissions(manage_messages=True)
    @hierarchy_check
    async def warn(
        self, ctx: Context, target: discord.Member, *, reason: str = "No reason given"
    ) -> t.Optional[discord.Message]:
        if await self.bot.database.fetch(
            "SELECT * FROM warns WHERE id = $1", target.id
        ):
            await self.bot.database.execute(
                """
                UPDATE warns
                SET data = data || ARRAY[($1, $2, $3)::warn_data] 
                WHERE id = $4
                """,
                ctx.message.id,
                ctx.author.id,
                reason,
                target.id,
            )
            return await ctx.reply(f"{target} warned for {reason}")

        await self.bot.database.execute(
            "INSERT INTO warns (id, data) VALUES ($1, ARRAY[($2, $3, $4)::warn_data])",
            target.id,
            ctx.message.id,
            ctx.author.id,
            reason,
        )
        await ctx.reply(f"{target} warned for {reason}")

    @commands.command()
    @commands.has_permissions(manage_messages=True)
    async def warns(
        self, ctx: Context, target: discord.Member
    ) -> t.Optional[discord.Message]:
        if data := await self.bot.database.fetchval(
            "SELECT data FROM warns WHERE id = $1", target.id
        ):
            list_embeds = []
            for warn in data:
                warn_id, warned_by, reason = warn
                embed = discord.Embed(colour=self.bot.config["BOT"]["colour"])
                embed.add_field(
                    name=f"Warn ID: {warn_id}",
                    value=f"Warned by: {warned_by}\nReason: ```{reason}```",
                )
                list_embeds.append(embed)

            return await HyperView(ctx, list_embeds).start()

        await ctx.reply(f"{target} has no warns")


def setup(bot: Bot) -> None:
    bot.add_cog(Mod(bot))
