from __future__ import annotations

from datetime import datetime
import typing as t

import nino

import discord
from discord.ext import commands

from ..core import Context
from ..utils import HyperView

if t.TYPE_CHECKING:
    from ..core import Bot


class Anilist(commands.Cog):
    def __init__(self, bot: Bot):
        self.bot = bot
        self.colour: int = self.bot.config["BOT"]["colour"]

        self.bot.loop.create_task(self.create_client())

    async def create_client(self) -> None:
        await self.bot.wait_until_ready()
        self.nino_client = nino.Client(session=self.bot.session)

    def base_embed(self, item: t.Any) -> discord.Embed:
        try:
            title = item.title
        except AttributeError:
            title = item.name

        if isinstance(title, dict):
            title = f"{title.get('first')} {title.get('last', '')}"

        if desc := item.description:
            desc = desc[:700] + "..." if len(desc) > 700 else desc

        else:
            desc = "..."

        embed = discord.Embed(title=title, description=desc, colour=self.colour)
        if item.url:
            embed.url = item.url
        return embed

    def humanize(self, date_: dict) -> t.Optional[str]:
        dt = "-".join([str(d) for d in date_.values() if d is not None])
        return dt if dt else None

    @commands.command(aliases=["sa"], help="Searches for given anime on anilist.")
    async def search_anime(self, ctx: Context, *, query: str) -> None:
        data = await self.nino_client.anime_search(query, per_page=6)
        embed_list = []
        for anime in data.animes:

            anime.__dict__["raw"]["title"] = anime.title.get("romaji")

            embed = self.base_embed(anime)

            if img := anime.banner_image.url:
                embed.set_image(url=img)

            if img := anime.cover_image.url:
                embed.set_thumbnail(url=img)

            embed.add_field(name="status:", value=anime.status)
            embed.add_field(name="episodes:", value=anime.episodes)
            embed.add_field(name="score:", value=anime.average_score)
            embed.add_field(name="tags:", value=", ".join(anime.tags))

            embed_list.append(embed)

        hyperv = HyperView(ctx, embed_list)
        await hyperv.start()

    @commands.command(aliases=["sc"], help="Searches for given character on anilist.")
    async def search_character(self, ctx: Context, *, query: str) -> None:
        data = await self.nino_client.character_search(query, per_page=6)
        embed_list = []
        for character in data.characters:

            embed = self.base_embed(character)

            if img := character.image.url:
                embed.set_thumbnail(url=img)

            embed.add_field(name="age:", value=character.age)
            embed.add_field(
                name="date of birth:", value=self.humanize(character.date_of_birth)
            )
            embed.add_field(name="gender:", value=character.gender)

            embed_list.append(embed)

        hyperv = HyperView(ctx, embed_list)
        await hyperv.start()

    @commands.command(aliases=["ss"], help="Searches for given staff on anilist.")
    async def search_staff(self, ctx: Context, *, query: str) -> None:
        data = await self.nino_client.staff_search(query, per_page=6)
        embed_list = []
        for staff in data.staffs:

            staff.__dict__["url"] = staff.image.url

            embed = self.base_embed(staff)

            if img := staff.image.url:
                embed.set_thumbnail(url=img)

            embed.add_field(name="age:", value=staff.age)
            embed.add_field(
                name="date of birth:", value=self.humanize(staff.date_of_birth)
            )
            embed.add_field(name="gender:", value=staff.gender)

            embed.add_field(name="language:", value=staff.language)
            embed.add_field(
                name="alive?", value=self.humanize(staff.date_of_death) or "Yes"
            )
            embed.add_field(name="id:", value=staff.id)

            embed_list.append(embed)

        hyperv = HyperView(ctx, embed_list)
        await hyperv.start()

    @commands.command(aliases=["su"], help="Searches for given user on anilist.")
    async def search_user(self, ctx: Context, *, query: str) -> None:
        data = await self.nino_client.user_search(query, per_page=6)
        embed_list = []
        for user in data.users:

            user.__dict__.update(dict(description=user.bio))

            embed = self.base_embed(user)

            if img := user.avatar.url:
                embed.set_thumbnail(url=img)

            if img := user.banner_image.url:
                embed.set_image(url=img)

            cat = user.raw.get("createdAt")
            uat = user.raw.get("updatedAt")

            embed.add_field(
                name="created at:",
                value=datetime.fromtimestamp(int(cat)) if cat else None,
            )
            embed.add_field(name="id:", value=user.id)
            embed.add_field(
                name="updated at:",
                value=datetime.fromtimestamp(int(uat)) if uat else None,
            )

            embed_list.append(embed)

        hyperv = HyperView(ctx, embed_list)
        await hyperv.start()


def setup(bot):
    bot.add_cog(Anilist(bot))
