from __future__ import annotations

import inspect
import typing as t

import discord
from discord.ext import commands

from ..utils import HyperView

if t.TYPE_CHECKING:
    from ..core import Bot


class CustomHelpCommand(commands.HelpCommand):
    def __init__(self, bot: Bot, **kwargs):
        super().__init__(**kwargs)
        self.bot = bot

    def __new__(
        cls: t.Type[CustomHelpCommand], *args, **kwargs
    ) -> CustomHelpCommand:  # Here to appease uvloop
        return super().__new__(cls)

    def copy(self):  # Here to appease uvloop
        obj = self.__class__(self.bot)
        obj._command_impl = self._command_impl
        return obj

    def get_command_signature(
        self, command: t.Union[commands.Command, commands.Group]
    ) -> str:
        cleaned_signature: t.List[str] = []
        cleaned_group_signature: t.Dict[str, list] = {}
        if isinstance(command, commands.Group):
            original_name = command.name
            for command in command.commands:
                cleaned_group_signature[command.name] = []
                for index, (name, param) in enumerate(
                    inspect.signature(command.callback).parameters.items()
                ):
                    if index == 0:
                        continue
                    elif index == 1:
                        continue

                    elif param.default is inspect._empty:  # type: ignore
                        cleaned_group_signature[command.name].append(f"<{name}>")
                    else:
                        cleaned_group_signature[command.name].append(f"[{name}]")

            for name, signature in cleaned_group_signature.items():
                cleaned_signature.append(f"\n{name}: `{' '.join(signature)}`")

            fmt = "".join(cleaned_signature)
            return f"{original_name}: Group commands {fmt}"

        for name, param in inspect.signature(command.callback).parameters.items():
            if param.default is inspect._empty:  # type: ignore
                cleaned_signature.append(f"<{name}>")
            else:
                cleaned_signature.append(f"[{name}]")

        return f"{command.name}: `{' '.join(cleaned_signature[2:])}`"  # type: ignore

    def generate_embed(self, data: t.List) -> discord.Embed:
        embed = discord.Embed(
            title="List of commands",
            description="`<>` -> Required arguments, `[]` -> Optional arguments",
            colour=self.bot.config["BOT"]["colour"],
        )
        embed.add_field(name=data[0], value=data[1], inline=True)

        return embed

    async def send_bot_help(self, mapping: dict) -> None:
        cleaned_items: t.List = []

        for cog, commands_ in mapping.items():
            if command := "\n".join(
                self.get_command_signature(c)
                for c in await self.filter_commands(commands_, sort=True)
            ):
                cleaned_items.append(
                    [getattr(cog, "qualified_name", "No Category"), command]
                )

        embeds: t.List[discord.Embed] = [
            self.generate_embed(data) for data in cleaned_items
        ]
        await HyperView(self.context, embeds).start()  # type: ignore

    async def send_command_help(self, command: commands.Command) -> None:
        embed = discord.Embed(
            colour=self.bot.config["BOT"]["colour"],
            description=self.get_command_signature(command),
        )
        embed.add_field(name="description:", value=command.help, inline=False)
        embed.set_footer(text=f"aliases: {', '.join(command.aliases)}")
        await self.get_destination().send(embed=embed)

    async def send_group_help(self, group: commands.Group) -> None:
        embed = discord.Embed(colour=self.bot.config["BOT"]["colour"])

        if group.help:
            embed.add_field(name="description:", value=group.help, inline=False)

        embed.add_field(
            name="commands:",
            value=self.get_command_signature(group)[len(group.name) + 16:],
            inline=False,
        )
        embed.set_footer(text=f"aliases: {', '.join(group.aliases)}")
        await self.get_destination().send(embed=embed)


class Help(commands.Cog):
    def __init__(self, bot: Bot):
        bot.help_command = CustomHelpCommand(bot, command_attrs={"hidden": True})
        self.bot = bot

    def cog_unload(self) -> None:
        self.bot.help_command = commands.DefaultHelpCommand()


def setup(bot) -> None:
    bot.add_cog(Help(bot))
