from __future__ import annotations

import typing as t
import functools

import discord
from discord.ext import commands
from jishaku.codeblocks import codeblock_converter

from ..core import Context

if t.TYPE_CHECKING:
    from ..core import Bot


class Admin(commands.Cog):
    def __init__(self, bot: Bot):
        self.bot = bot

    async def cog_check(self, ctx: Context) -> bool:
        """
        A check that is ran before every command in the cog.

        Args:
            ctx (Context): The invocation context
        """
        return await commands.is_owner().predicate(ctx)

    @commands.command()
    async def blacklist(
        self,
        ctx: Context,
        target: t.Union[discord.Guild, discord.User],
        reason: str = "No reason given",
    ) -> None:
        """
        A command used to blacklist a guild/user from using the bot

        Args:
            ctx (Context): The invocation context
            target (t.Union[discord.Guild, discord.User]): The target to blacklist
            reason (str): The reason for the blacklist
        """
        await ctx.database.execute(
            "INSERT INTO blacklist (id, reason) VALUES ($1, $2)", target.id, reason
        )
        await ctx.reply(
            embed=discord.Embed(
                colour=self.bot.config["BOT"]["colour"],
                description=f"Blacklisted {target}",
            )
        )

    @commands.command()
    async def unblacklist(
        self, ctx: Context, target: t.Union[discord.Guild, discord.User]
    ) -> None:
        """
        A command used to unblacklist a guild/user from using the bot

        Args:
            ctx (Context): The invocation context
            target (t.Union[discord.Guild, discord.User]): The target to unblacklist
        """
        await ctx.database.execute("DELETE FROM blacklist WHERE id = $1", target.id)
        await ctx.reply(
            embed=discord.Embed(
                colour=self.bot.config["BOT"]["colour"],
                description=f"Unblacklisted {target}",
            )
        )

    @commands.command(aliases=["e", "exec"])
    async def eval(
        self, ctx: Context, *, code: codeblock_converter  # type: ignore
    ) -> None:
        """
        A point command for `jsk py`, used to evaluate code.
        """
        if self.eval_command is not None:
            await self.eval_command(ctx, argument=code)

    @functools.cached_property
    def eval_command(self) -> t.Union[commands.Command, None]:
        if command := self.bot.get_command("jsk py"):
            return command

        return None


def setup(bot: Bot) -> None:
    bot.add_cog(Admin(bot))
