from __future__ import annotations

import typing as t

import discord
from discord.ext import commands

from ..core import Context

if t.TYPE_CHECKING:
    from ..core import Bot


class Config(commands.Cog):
    def __init__(self, bot: Bot):
        self.bot = bot

    async def cog_check(self, ctx: Context) -> bool:
        return await commands.guild_only().predicate(ctx)

    @commands.group(invoke_without_command=True)
    @commands.has_permissions(manage_guild=True)
    async def set(self, ctx: Context) -> None:
        """
        A set of commands to set configs per guild
        """
        await ctx.send_help("set")

    @set.command()
    async def mute(self, ctx: Context, role: discord.Role) -> None:
        """
        A command that sets the guild's role to be used when muting
        """
        await self.bot.database.execute(
            "UPDATE guilds set mute_role = $1 WHERE id = $2",
            role.id,
            ctx.guild.id,  # type: ignore
        )
        await ctx.reply(f"Set the servers muted role to `{role}`")

    @set.command()
    async def prefix(
        self, ctx: Context, prefix: t.Optional[str] = None
    ) -> t.Optional[discord.Message]:
        """
        A command that sets the guild's prefix.
        If no prefix is given it will reset to the default one.
        """
        if prefix is None:
            await self.bot.database.execute(
                "UPDATE guilds SET prefix = $1 WHERE id = $2",
                self.bot.config["BOT"]["prefix"],
                ctx.guild.id,  # type: ignore
            )
            return await ctx.reply("Prefix resetting to default one")

        await self.bot.database.execute(
            "UPDATE guilds SET prefix = $1 WHERE id = $2",
            prefix,
            ctx.guild.id,  # type: ignore
        )
        await ctx.reply(f"Prefix changed to `{prefix}`")


def setup(bot: Bot) -> None:
    bot.add_cog(Config(bot))
