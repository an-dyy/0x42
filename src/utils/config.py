from __future__ import annotations

import typing as t

import yaml

__all__ = ("Config",)


class Config:
    """
    Used to get the configurations from a yaml file.

    Attributes:
        data (t.Dict[str, t.Any]): The data from the yaml file
    """

    def __init__(self, file: str):
        with open(file) as f:
            self.data = yaml.load(f, Loader=yaml.Loader)

    @classmethod
    def from_file(cls: t.Type[Config], file: str) -> Config:
        """
        A classmethod which creates the instance.

        Returns:
            The created instance from a file path
        """
        return cls(file)

    def __getitem__(self, key: t.Union[str, int]) -> t.Any:
        return self.data[key]
