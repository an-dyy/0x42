import typing as t

import discord
from discord.ext import commands

__all__ = ("HyperView",)


class HyperView(discord.ui.View):
    STYLE = discord.ButtonStyle.blurple
    LABELS = {
        "fast_forward": "↗️",
        "forward": "➡️",
        "stop": "◻️",
        "backward": "⬅️",
        "rewind": "↙️",
    }

    def __init__(
        self, ctx: commands.Context, pages: t.List[discord.Embed], **kwargs
    ) -> None:
        super().__init__(**kwargs)
        self.pages = pages
        self.ctx = ctx
        self.current_page: int = 0

    async def interaction_check(self, interaction: discord.Interaction) -> bool:
        if self.ctx.author.id != interaction.user.id:  # type: ignore

            await interaction.response.send_message(
                "You cannot interact with other peoples menu", ephemeral=True
            )
        return True

    async def _edit(self, interaction: discord.Interaction) -> None:
        try:
            embed = self.pages[self.current_page]
            embed.set_footer(text=f"Page: {self.current_page+1}")
            await interaction.response.edit_message(embed=embed)
        except IndexError:
            pass

    async def start(self) -> discord.Message:
        embed = self.pages[self.current_page]
        embed.set_footer(text=f"Page: {self.current_page+1}")
        return await self.ctx.send(embed=embed, view=self)

    @discord.ui.button(label=LABELS["rewind"], style=STYLE)
    async def rewind(
        self, button: discord.ui.Button, interaction: discord.Interaction
    ) -> None:
        self.current_page = 0
        await self._edit(interaction)

    @discord.ui.button(label=LABELS["backward"], style=STYLE)
    async def backward(
        self, button: discord.ui.Button, interaction: discord.Interaction
    ) -> None:
        self.current_page -= 1 if self.current_page > 0 else 0
        await self._edit(interaction)

    @discord.ui.button(label=LABELS["stop"], style=STYLE)
    async def stop_interaction(
        self, button: discord.ui.Button, interaction: discord.Interaction
    ) -> None:
        await interaction.response.send_message("Stopped Interactions.", ephemeral=True)
        self.stop()

    @discord.ui.button(label=LABELS["forward"], style=STYLE)
    async def foward(
        self, button: discord.ui.Button, interaction: discord.Interaction
    ) -> None:
        self.current_page += 1 if self.current_page < len(self.pages) else 0
        await self._edit(interaction)

    @discord.ui.button(label=LABELS["fast_forward"], style=STYLE)
    async def fast_forward(
        self, button: discord.ui.Button, interaction: discord.Interaction
    ) -> None:
        self.current_page = len(self.pages) - 1
        await self._edit(interaction)
