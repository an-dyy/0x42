from __future__ import annotations

from discord.ext import commands

__all__ = ("HierarchyFail",)


class HierarchyFail(commands.CheckFailure):
    ...
