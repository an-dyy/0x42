from __future__ import annotations

import typing as t
import datetime
import re

from discord.ext import commands

if t.TYPE_CHECKING:
    from ..core import Context

T = t.TypeVar("T")

__all__ = ("convert", "TimeConverter",)


class Time(t.NamedTuple):
    time: datetime.timedelta
    readable: str
    raw: int


class TimeConverter(commands.Converter):
    TIME_MAPPING: t.ClassVar[t.Dict[str, int]] = {
        "s": 1,
        "m": 60,
        "h": 3600,
        "d": 86400,
    }

    async def convert(self, ctx: Context, argument: str) -> Time:
        if re.search(r"\d", argument):
            _, time, unit = re.split(r"(\d+)", argument)
            if unit in TimeConverter.TIME_MAPPING:
                seconds = TimeConverter.TIME_MAPPING[unit] * int(time)
                return Time(datetime.timedelta(seconds=seconds), argument, seconds)
            raise commands.BadArgument("Not a valid time unit")
        else:
            raise commands.BadArgument("Not a valid time")


async def convert(ctx: Context, id: int, *, cls: t.Type[T]) -> t.Union[T, None]:
    """
    A method to convert an ID to a discord object

    Args:
        ctx (Context): Invocation context
        id (int): The ID to convert into an object
        cls (t.Type[T]): The type of the object to return

    Returns:
        The object converted from the ID or None if not found
    """
    try:
        if ret := await commands.converter.CONVERTER_MAPPING[cls]().convert(
            ctx, str(id)
        ):
            return ret
        else:
            return None
    except commands.BadArgument:
        return None
