from __future__ import annotations

import typing as t
import functools

import discord

from .errors import HierarchyFail

__all__ = ("hierarchy_check",)


def hierarchy_check(func: t.Callable) -> t.Callable[..., t.Coroutine]:
    @functools.wraps(func)
    async def inner(*args, **kwargs) -> t.Union[t.Coroutine]:
        list_of_bools = [
            args[1].author.top_role > target.top_role
            for target in args
            if type(target) is discord.Member
        ]
        if all(list_of_bools):
            return await func(*args, **kwargs)  # type: ignore
        else:
            raise HierarchyFail("Cannot run action on this user")

    return inner
