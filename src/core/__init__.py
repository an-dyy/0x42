import sys
import os

if sys.platform == "linux":
    import uvloop

    uvloop.install()

from .botbase import *
from .database import *
from .context import *

os.environ["JISHAKU_NO_DM_TRACEBACK"] = "True"
os.environ["JISHAKU_HIDE"] = "True"
