from __future__ import annotations

import typing as t

import discord
from discord.ext import commands

from ..utils import convert

if t.TYPE_CHECKING:
    from .database import Database

__all__ = ("Context",)

T = t.TypeVar("T")


class Context(commands.Context):
    """
    An extended Context class

    Attributes:
        database (Database): The database which is connected to the bot
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.database: Database = self.bot.database

    async def reply(
        self, content: t.Optional[str] = None, mention_author: bool = False, **kwargs
    ) -> discord.Message:
        """
        This method exists because I don't want to mention the author everytime.
        """
        return await super().reply(content, mention_author=mention_author, **kwargs)

    async def convert(
        self, id_: int, *, cls: t.Type[T] = discord.Object
    ) -> t.Union[T, None]:
        """
        A shorthand for await util.converter(...) here for ease of access
        """
        return await convert(self, id_, cls=cls)
