from __future__ import annotations

import os
import logging
import traceback

import aiohttp
import datetime
import typing as t

import discord
from discord.ext import commands

from .database import Database
from .context import Context
from ..utils import Config

__all__ = ("Bot",)

CT = t.TypeVar("CT", bound=commands.Context)


class _0x42(commands.Bot):
    """
    A bot meant to whatever. Just a project to pass some time.
    Everything used is free to take but I, would appreciate if you credit for used snippets

    Attributes:
        startup (datetime.datetime): The `datetime.datetime` instance corresponding to startup time
        config (Config): The `Config` instance used to access configurations set inside of `config.yml`
        owner_ids (t.List[int]): The list of owner ids belonging to the bot
        session (aiohttp.ClientSession): The `aiohttp.ClientSession` the bot will use for APIs
        database (Database): The `Database` instance` used to interact with the postgres database
        logger (logging.Logger): The logger to use for the bot
    """

    def __init__(self, *args, **kwargs):
        intents = discord.Intents.default()
        intents.members = True
        super().__init__(
            self.get_prefix,
            intents=intents,
            case_insensitive=True,
            allowed_mentions=discord.AllowedMentions(everyone=False, roles=False),
            *args,
            **kwargs,
        )
        self.startup: datetime.datetime = datetime.datetime.utcnow()
        self.config: Config = Config.from_file("config.yml")
        self.owner_ids: t.List[int] = self.config["BOT"]["owner_ids"]
        self.muted_users: t.Dict[int, t.Tuple[datetime.datetime, int, int]] = {}
        self.session: aiohttp.ClientSession
        self.database: Database
        self.logger: logging.Logger

        self.load_extension("jishaku")
        self.add_check(self.check_blacklist)  # type: ignore
        self.setup_logs()

    def setup_logs(self) -> None:
        handler = logging.StreamHandler()
        handler.setFormatter(
            logging.Formatter("[%(levelname)s] -> %(name)s: %(message)s")
        )
        self.logger = logging.getLogger(__name__)
        self.logger.addHandler(handler)
        self.logger.setLevel(logging.INFO)

    async def get_prefix(self, message: discord.Message) -> str:
        """
        A method called to determine the prefix.

        Args:
            message (discord.Message): The `discord.Message` instance which has the information needed

        Returns:
            The str/prefix corresponding to where the message was sent
        """
        if message.guild is not None:
            if prefix := await self.database.fetchrow(
                "SELECT prefix FROM guilds WHERE id = $1", message.guild.id
            ):
                return prefix[0]
            else:
                return self.config["BOT"]["prefix"]

        return self.config["BOT"]["prefix"]

    async def get_context(
        self, message: discord.Message, *, cls: t.Type[CT] = Context
    ) -> CT:
        return await super().get_context(message, cls=cls)

    async def on_guild_join(self, guild: discord.Guild) -> str:
        """
        A method called whenever the bot joins a guild.
        Used to append data to the guilds table of the database.

        Args:
            guild (discord.Guild): The `discord.Guild` instance of the guild which the bot just joined

        Returns:
            The status of the command
        """
        return await self.database.execute(
            "INSERT INTO guilds (id, prefix) VALUES ($1, $2)",
            guild.id,
            str(self.config["BOT"]["prefix"]),
        )

    async def on_guild_remove(self, guild: discord.Guild) -> str:
        """
        A method called whenever the bot is removed from a guild.
        Used to delete data from the guilds table of the database.

        Args:
            guild (discord.Guild): The `discord.Guild` instance of the guild which the bot was removed from

        Returns:
            The status of the command
        """
        return await self.database.execute("DELETE FROM guilds WHERE id = $1", guild.id)

    async def check_blacklist(self, ctx: Context) -> bool:
        """
        Checks whether or not to process the command depending on if the author/guild is blacklisted or not.

        Args:
            ctx (Context): Invocation context

        Returns:
            A bool for whether or not they pass the check
        """
        if await self.database.fetchrow(
            "SELECT * FROM blacklist WHERE id = $1", ctx.guild.id if ctx.guild else None
        ):
            return False

        elif await self.database.fetchrow(
            "SELECT * FROM blacklist WHERE id = $1", ctx.author.id
        ):
            return False

        else:
            return True

    async def populate_mutes(self) -> None:
        if data := await self.database.fetch(
            "SELECT target_id, mute_at::timestamp, mute_seconds, guild_id FROM mutes"
        ):
            [
                self.muted_users.update({mute[0]: (mute[1], mute[2], mute[3])})
                for mute in data
            ]

    async def on_command_error(
        self, ctx: Context, error: commands.CommandError
    ) -> None:
        """
        A method used to handle errors

        Args:
            ctx (commands.Context): Context corresponding to errors invocation
            error (commands.CommandError): The error raised
        """
        trace = traceback.format_exception(type(error), error, error.__traceback__)
        return self.logger.error(f"{''.join(trace)}")

    async def on_ready(self) -> None:
        """
        Self explainatory, ran when the bot is ready.
        """
        self.logger.info(f"BOT READY: {self.user.id}")  # type: ignore

    async def start(self, *args, **kwargs) -> None:
        """
        A method used to run the bot.
        """
        for ext in [
            f"src.exts.{ext[:-3]}"
            for ext in os.listdir("src/exts")
            if ext.endswith(".py") and not ext.startswith("_")
        ]:
            self.load_extension(ext)
            self.logger.info(f"LOADED: {ext}")

        self.session = aiohttp.ClientSession(loop=self.loop)
        self.logger.info("CREATED: session")

        try:
            self.database = await Database.create_pool(self.config)
            await self.populate_mutes()
            self.logger.info(f"DATABASE READY: {self.config['DB']['name']}")
        except Exception as e:
            raise e

        return await super().start(self.config["BOT"]["token"], *args, **kwargs)

    async def close(self) -> None:
        """
        A method used to close things when the bot is closed.
        """
        await self.session.close()
        await self.database.pool.close()
        await super().close()

    @property
    def avatar(self) -> discord.Asset:
        return self.user.avatar  # type: ignore

    @property
    def uptime(self) -> datetime.timedelta:
        return self.startup - datetime.datetime.utcnow()

    @property
    def owners(self) -> t.List[discord.User]:
        return [user for id_ in self.owner_ids if (user := self.get_user(id_))]


Bot = _0x42
