from __future__ import annotations

import typing as t

import asyncpg

if t.TYPE_CHECKING:
    from ..utils import Config

__all__ = ("Database",)


class Database:
    """
    Used to interface the postgresql database.

    Attributes:
        pool (asyncpg.Pool): the `asyncpg.Pool` used to receive and send data from
    """

    def __init__(self):
        self.pool: asyncpg.Pool

    @classmethod
    async def create_pool(cls: t.Type[Database], config: Config) -> Database:
        """
        A classmethod to create the database and the pool connected to it.
        As well as creating any missing tables.

        Returns:
            The created instance
        """
        self = cls()
        self.pool = await asyncpg.create_pool(  # type: ignore
            database=config["DB"]["name"],
            user=config["DB"]["user"],
            password=config["DB"]["password"],
        )

        with open("schema.sql") as file:
            await self.pool.execute(file.read())  # type: ignore

        return self

    async def execute(self, query: str, *args: t.Any, timeout: float = None) -> str:
        """
        A method used to execute queries on the pool.

        Args:
            query (str): The actual query to be executed
            args (t.Any): The query args

        Returns:
            The status of the executed command

        """
        async with self.pool.acquire() as connection:
            return await connection.execute(query, *args, timeout=timeout)

    async def fetch(self, query: str, *args: t.Any, timeout: float = None) -> list:
        """
        A method used to fetch data from the pool.

        Args:
            query (str): The query to fetch with
            args (t.Any): The query args

        Returns:
            A list of returned data
        """
        async with self.pool.acquire() as connection:
            return await connection.fetch(query, *args, timeout=timeout)

    async def fetchval(
        self, query: str, *args, column: int = 0, timeout: float = None
    ) -> t.Any:
        """
        A method used to fetch the value of the query from the first row
        """
        async with self.pool.acquire() as connection:
            return await connection.fetchval(
                query, *args, column=column, timeout=timeout
            )

    async def fetchrow(
        self, query: str, *args, timeout: float = None
    ) -> asyncpg.Record:
        """
        A method used to fetch the first row from a query

        Args:
            query (str): The query to fetch from
            args: (t.Any): The query args

        Returns:
            The `asyncpg.Record` instance corresponding to the data returned
        """
        async with self.pool.acquire() as connection:
            return await connection.fetchrow(query, *args, timeout=timeout)
