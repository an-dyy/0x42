---
name: Bug report
about: Report a bug
title: ''
labels: ''
assignees: an-dyy

---

**Summary of the bug**
Explain what is going on. Be clear when explaining

**Reproduction**
Minimal reproduction code:
```py
# example: 1/0
```

**Expected behavior**
A clear and concise description of what you expected to happen.

**Screenshots**
If applicable, add screenshots to help explain your problem.

**Information**
 - OS: [e.g Linux, Windows]
 - VERSION: [e.g v1.0]

**Additional context**
Add any other context about the problem here.
