# Information/Details 
_**IMPORTANT:** Only open an PR if you have discussed with me or otherwised open an issue for discussion. I will reject PR's that don't do this beforehand._

### Summary
_Explain what the PR does, make sure to be very clear._

### Usage/Examples
_Show a usage/example of whatever code you are adding. Make sure its not a niche use case._

### Closes issues
_Does this PR fix any issues? If so reference them here._

# Required
**Code needs to pass flake8 lint check**
**Make sure to write clean code that follows our [style guide](https://github.com/an-dyy/0x42/blob/master/STYLE-GUIDE.md)**
