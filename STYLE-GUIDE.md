### Style guide
_Used to keep code consistent, clean and readable_

# General
* Indentation should be 4 spaces
* Docstring the methods/classes you are adding
* Add return annotations to methods/functions
* Typehint methods etc, I would suggest getting a typecheck liker pyright
* Quotes are double quotes, not single unless unavoidable i.e `f"{data['key']}"`

# Formatters
_This repo uses [black](https://github.com/psf/black) as formatter_

Make sure `line-length` is set to 120

# Commit messages
_This repo follows [Conventional commits](https://www.conventionalcommits.org/en/v1.0.0/)_

# How to write embeds
If the embed is small enough
```py
await ctx.reply(
    embed=discord.Embed(
    colour=self.bot.config["BOT"]["colour"],
    description=f"Unblacklisted {target}",
   )
)
```
If the embed is big
```py
embed = discord.Embed(..., #etc)
embed.add_field(name=name, value=value, inline=True)
# etc

await ctx.reply(embed=embed)
```
# Convention
_This repo follows pep8_
Make sure to follow the naming conventions

# Imports
Keep imports to a minimum, don't add any extra dependancies to the bot unless otherwise approved by me.

Imports should look like
```py
import src


src.SomeClass(...)
```

Imports should not look like
```py
from src import SomeClass

SomeClass(...)
```
Unless otherwise unavoidable
