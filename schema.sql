CREATE TABLE IF NOT EXISTS guilds (
    id BIGINT PRIMARY KEY,
    prefix TEXT,
    mute_role BIGINT DEFAULT 0
);

CREATE TABLE IF NOT EXISTS blacklist (
    id BIGINT NOT NULL,
    reason VARCHAR(250) NOT NULL
);

CREATE TABLE IF NOT EXISTS mutes (
    target_id BIGINT NOT NULL,
    guild_id BIGINT,
    mute_at TIMESTAMP,
    mute_seconds INT
);

DO $$
BEGIN
IF NOT EXISTS (SELECT * FROM pg_type WHERE typname = 'warn_data') THEN
    CREATE TYPE warn_data AS (
        warn_id BIGINT,
        warned_by BIGINT,
        reason TEXT
    );
END IF;

END$$;
CREATE TABLE IF NOT EXISTS warns (
    id BIGINT PRIMARY KEY NOT NULL,
    data warn_data[]
);
